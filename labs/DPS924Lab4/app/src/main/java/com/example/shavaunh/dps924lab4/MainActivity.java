package com.example.shavaunh.dps924lab4;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        FragmentOne f1 = new FragmentOne();
        FragmentTwo f2 = new FragmentTwo();

        ft.add(R.id.frag_view,f1,"Fragment1");
        ft.add(R.id.frag_view,f2,"Fragment2");
        ft.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SwitchImage(View v){
        ImageView imagecontainer1 = (ImageView)findViewById(R.id.image_one);
        ImageView imagecontainer2 = (ImageView)findViewById(R.id.image_two);
        Drawable src1= imagecontainer1.getDrawable();
        Drawable src2= imagecontainer2.getDrawable();

        imagecontainer1.setImageDrawable(src2);
        imagecontainer2.setImageDrawable(src1);
    }
}
