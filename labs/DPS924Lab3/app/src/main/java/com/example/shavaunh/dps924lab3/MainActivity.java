package com.example.shavaunh.dps924lab3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import Shavauhn.Debugging;

public class MainActivity extends AppCompatActivity {

    static String Button_Clicked = "Button Clicked";
    Debugging d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public void onResume(){
        super.onResume();
        d = new Debugging(this.getResources());
    }

    @Override
    public void onPause(){
        super.onPause();
        d = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        Intent i;
        switch (item.getTitle().toString()){
            case "Help":
                i = new Intent(this,Help.class);
                startActivity(i);
                break;
            case "About":
                i = new Intent(this,About.class);
                startActivity(i);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    //Events


    public void LoadTerminology(View b){
        Button button = (Button)b;
        d.MainPage(Button_Clicked);
        d.Websites("Navigating to the terminology page");
        startActivity(new Intent(getApplication(),Terminology.class));
    }

    public void LoadWebSite(View b){
        Button button = (Button)b;
        d.MainPage(Button_Clicked);
        d.Websites("Navigating to the website page");
    }


}
