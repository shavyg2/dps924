package com.example.shavaunh.dps924lab3;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Terminology extends AppCompatActivity {
    static String TAG = "Shavauhn.Terminology";

    EditText SearchBox;
    ListView SearchListView;

    String[] Terms;
    ArrayList<String> displayTerms;
    ArrayAdapter<String> List;
    Handler h;
    Date ToastStart;


    class FilterSearch extends AsyncTask<String,Void,Integer>{


        class MultiSearch implements Runnable{
            int slen;
            String search;
            int i;
            int completed=0;
            int numTerms;

            public MultiSearch(String search,String[] terms){
                slen = search.length();
                this.search=search;
                numTerms = terms.length;
                i=0;
            }

            public void run() {
                if(Terms==null){
                    return;
                }
                String test = Terms[i++];
                //Not case sensitive
                String pattern;
                Pattern p;
                Matcher m;

                //No need to process alot
                if(test==search){
                    displayTerms.add(test);
                    done();
                    return;
                }
                //Fuzzy searching incase of mistypes
                else if(slen >=2){
                    for(int i=0;i<slen;i++){
                        if(i>0){
                            pattern= search.substring(0,i)+".?"+search.substring(i,slen);
                        }else{
                            pattern = ".?"+search.substring(i,slen);
                        }
                        p = Pattern.compile(pattern);
                        m = p.matcher(test);
                        if(m.find()){
                            displayTerms.add(test);
                            done();
                            return;
                        }
                    }

                    for(int i=0;i<slen;i++){
                        StringBuilder _search= new StringBuilder();
                        for(int j=0;j<slen;j++){
                            if(j==i){
                                _search.append(".?");
                            }else{
                                _search.append(search.charAt(j));
                            }
                        }
                        pattern = _search.toString();
                        p = Pattern.compile(pattern);
                        m = p.matcher(test);
                        if(m.find()){
                            displayTerms.add(test);
                            done();
                            return;
                        }
                    }
                }
                //No need for heavy Processing
                else{
                    displayTerms.add(test);
                    done();
                    return;
                }

                done();
            }

            public void done(){
                completed++;
                if(completed==numTerms){
                    h.post(new Runnable() {
                        @Override
                        public void run() {
                            List.notifyDataSetChanged();
                        }
                    });

                }
            }
        }


        @Override
        protected Integer doInBackground(String... params) {
            final String search = params[0];

            Vector<String> filtered= new Vector<String>();
            displayTerms.clear();
            //Useful as search terms increase. Allows for cpu intense fuzzy search later
            //Currently Overkill
            ExecutorService multicoreSearchService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
            int i,len;
            Runnable r = new MultiSearch(search,Terms);
            for(i=0,len=Terms.length;i<len;i++){
                multicoreSearchService.execute(r);
            }

            multicoreSearchService.shutdown();
            return 0;
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terminology);
        LoadTerms();
        displayTerms = new ArrayList<String>();
        Arrays.sort(Terms);
        Log.d(TAG,"Loaded Terms for display");

        List = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,displayTerms);

        //Handle Search Content
        SearchBox = (EditText) (findViewById(R.id.search_box));
        int limit = 0;

        for(int i=0,len=Terms.length;i<len;i++){
            limit=Math.max(limit, Terms[i].length());
        }

        SearchBox.setMaxLines(1);
        SearchBox.setFilters(new InputFilter[]{new InputFilter.LengthFilter(limit)});

        SearchBox.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP) {
                    Log.d(TAG, "Key Up was performed");
                    PerformSearch();
                }
                return false;
            }
        });

        Log.d(TAG, "Set Key Listener");
        SearchListView = (ListView) findViewById(R.id.search_list);
        SearchListView.setAdapter(List);
        SearchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView item = (TextView) view;
                String item_text= item.getText().toString();

                String ItemName= item_text.replace(' ', '_');
                String ItemDescription;
                try {
                    ItemDescription = getString(getResources().getIdentifier(ItemName, "string", getPackageName()));

                }catch(Exception e){
                    Log.e(TAG,e.getMessage());
                    Log.e(TAG,"Mistake not getting the correct identifier");
                    Log.e(TAG,"The item name is "+ItemName);
                    throw new RuntimeException("This needs to be fixed.");
                }

//                if(ToastStart == null) {
//                  ToastStart = new Date();
                    Toast t = Toast.makeText(getApplicationContext(), ItemDescription, Toast.LENGTH_LONG);
                    t.show();
//                }
            }
        });
        h = new Handler(Looper.getMainLooper());
    }

    @Override
    public void onPause(){
        super.onPause();
        //Free memory where we can
        displayTerms.clear();
        unLoadTerms();
    }

    @Override
    public void onStop(){
        //Prolly not needed
        super.onStop();
        unLoadTerms();
    }


    @Override
    public void onResume(){
        super.onResume();
        LoadTerms();
        PerformSearch();
    }


    public void PerformSearch(){
        String searchTerm = ((EditText) findViewById(R.id.search_box)).getText().toString();
        Log.d(TAG, "Search term was " + searchTerm);
        new FilterSearch().execute(searchTerm);
    }


    public void LoadTerms(){
        if(Terms==null)
        Terms = getResources().getStringArray(R.array.terminology);
    }

    public void unLoadTerms(){
        if(Terms!=null)
            Terms = null;
    }
}
