package Shavauhn;

import android.content.res.Resources;
import android.util.Log;

import com.example.shavaunh.dps924lab3.R;

import java.util.ResourceBundle;

/**
 * Created by Shavaunh on 9/28/2015.
 */
public class Debugging {

    Resources resource;
    public Debugging(Resources r){
        resource=r;
    }

    public void Log(String Tag,String message){
        Log.d(Tag,message);
    }

    public void MainPage(String message){
        Log(resource.getString(R.string.MainPage), message);
    }

    public void Terminology(String message){
        Log(resource.getString(R.string.Terminology),message);
    }

    public void Websites(String message){
        Log(resource.getString(R.string.Websites),message);
    }

    public void debug(String message){
        Log(resource.getString(R.string.debug),message);
    }





}
