package com.example.shavaunh.dps924lab6.Shavauhn;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.orm.SugarRecord;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Shavaunh on 10/1/2015.
 */
public class Friend extends SugarRecord<Friend> {
    private static String TAG="Shavauhn.Friend";
    public String FirstName;
    public String LastName;

    public String Phone;
    public String Email;
    static protected Context ctx;
    public static ArrayList<Runnable> runnables= new ArrayList<Runnable>();
    public static ArrayList<Handler> handles = new ArrayList<Handler>();

    public Friend(){}

    public Friend(String fname,String lname,String phone,String email){
        this.FirstName = fname;
        this.LastName = lname;
        this.Phone = phone;
        this.Email = email;
    }

    public static void Populate(Context ctx){
        Friend.ctx=ctx;
        String json = ReadJSON();

        JSONArray jj=null;
        try {
            jj = new JSONArray(json);
        }catch (JSONException e){

        }
        class Counter{
            int count=0;
        }

        final Counter counter = new Counter();
        final JSONArray j= jj;
        final Context context = ctx;
//        ExecutorService e = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors()*2);
            for(int i=0,len=j.length();i<len;i++) {
                final int index = i;
//                Runnable r = new Runnable() {
//                    @Override
//                    public void run() {
                        try {
                            JSONObject obj = (JSONObject) j.get(index);
                            Friend friend = new Friend();
                            friend.FirstName = obj.getString("fname");
                            friend.LastName = obj.getString("lname");
                            friend.Phone = obj.getString("phone");
                            friend.Email=obj.getString("email");
                            friend.save();
                        } catch (JSONException e) {

                        }

//                        counter.count++;
//                        if(counter.count==j.length()){
//                            for(int i=0,len=runnables.size();i<len;i++){
//                                handles.get(i).post(runnables.get(i));
//                            }
//                            handles.clear();
//                            runnables.clear();
//                        }
//                    }
//                };

//                e.execute(r);
            }
//        e.shutdown();
    }

    public static void onPopulate(Runnable r,Handler h){
        runnables.add(r);
        handles.add(h);

    }

    public static String ReadJSON(){
        StringBuffer sb = new StringBuffer();

        try {
            InputStream s = ctx.getAssets().open("friends_list.json");
            InputStreamReader isr = new InputStreamReader(s);

            int data=isr.read();
            char c;
            while(data!=-1){
                c = (char)data;
                sb.append(c);
                data = isr.read();
            }

            isr.close();
            s.close();
        }catch(FileNotFoundException e){
            Log.e(TAG,"No JSON files");
        }catch(IOException e){
            Log.e(TAG,"friend list not found or something");
        }catch(Exception e){

        }finally {

        }

        return sb.toString();
    }

    public static boolean IsEmpty(){

        boolean empty=true;
        try {
            List<Friend> friends = Friend.listAll(Friend.class);
            empty = friends.size() == 0;
        }catch(IllegalStateException e){

        }catch(ExceptionInInitializerError e){

        }
        catch(Exception e){
            empty=true;
        }
        return empty;
    }
}
