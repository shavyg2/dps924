package com.example.shavaunh.dps924lab6;

import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Formatter;

import com.example.shavaunh.dps924lab6.Shavauhn.Friend;
import com.orm.StringUtil;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    boolean stop = false;
    Handler dumbDispatcher;

    final String TAG = "Shavauhn";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dumbDispatcher = new Handler(Looper.getMainLooper());

        new Thread() {

            public void run() {
                if (Friend.IsEmpty()) {
                    Log.d(TAG, "No Friends in Database. Populating now");

                    Friend.Populate(getApplicationContext());
                    dumbDispatcher.post(new Runnable() {
                        @Override
                        public void run() {
                            Ready();
                        }
                    });
                } else {
                    dumbDispatcher.post(new Runnable() {
                        @Override
                        public void run() {
                            Ready();
                        }
                    });
                }
            }
        }.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void Ready() {

        Log.d(TAG, "Finished Populating Database. Querying and Populating ListView");
        ListView friendsList = (ListView) findViewById(R.id.friends_list);
        List<Friend> friends = Friend.listAll(Friend.class);
        ArrayList<String> friendNames = new ArrayList<String>();
        for (Friend f : friends) {
            friendNames.add(f.FirstName + " " + f.LastName);
        }

        ArrayAdapter<String> list = new ArrayAdapter<String>(getApplicationContext(), R.layout.friend_list_item, friendNames);
        friendsList.setAdapter(list);

        friendsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Formatter format = new Formatter();
                TextView t = (TextView) view;
                String name = t.getText().toString();
                String query = format.format("%s = ? and %s = ?", StringUtil.toSQLName("FirstName"), StringUtil.toSQLName("LastName")).toString();
                Log.d(TAG, query);
                Friend friend = Friend.find(Friend.class, query, name.split(" ")[0], name.split(" ")[1]).get(0);
                String message = new Formatter().format("%s %s %s %s", friend.FirstName, friend.LastName, friend.Email, friend.Phone).toString();
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
