package com.example.shavaunh.dps924lab5;

import android.content.Context;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    protected boolean stop=false;
    final String TAG = "Shavauhn";
    Handler DumbDispatcher;
    ArrayList<String> Colors;
    ArrayAdapter<String> ColorAdapter;
    ListView ColorPicker;
    Thread ColorUpdater;
    String CurrentTextViewColor;
    final int white = Color.WHITE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        DumbDispatcher = new Handler(Looper.getMainLooper());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoadColors();

        Log.d(TAG, "The size of the colors list is " + Colors.size());

        ColorAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.list_text,Colors);
        ColorPicker = (ListView)findViewById(R.id.color_picker);
        ColorPicker.setAdapter(ColorAdapter);

        ColorPicker.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView t = (TextView) view;
                UpdateColor(t.getText().toString());
            }
        });
        registerForContextMenu(ColorPicker);
        ReadSave();
    }

    public void UpdateColor(String c){
        CurrentTextViewColor = c;
        int color = Color.parseColor(c);

        //Possible to do something cool manipulate some colors to make readable
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        ColorPicker.setBackgroundColor(color);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        menu.setHeaderTitle("Select The Action");
        menu.add(0, v.getId(), 0, getString(R.string.write_to_sd));
        menu.add(0, v.getId(), 0, getString(R.string.read_from_sd));
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {

        String title = item.getTitle().toString();
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();


        Log.d(TAG, "Obtained " + title + " from content menu");
        if(title == getString(R.string.write_to_sd)){
            Save(((TextView)(info.targetView)).getText().toString());
        }else if(title == getString(R.string.read_from_sd)){
            ReadSave();
        }else{
            Log.d(TAG,"Messed up something");
        }

        return true;
    }


    public void Save(String color){
        UpdateColor(color);
        File colorPath;
        try{
            colorPath = Environment.getExternalStorageDirectory();
        }catch(Exception e){
            colorPath = Environment.getDataDirectory();
        }

        Log.d(TAG,"writing to "+colorPath.getAbsolutePath());

            try {
                colorPath = new File(colorPath,getString(R.string.filedir));
                colorPath.mkdirs();
                colorPath= new File(colorPath,getString(R.string.filepath));
                colorPath.createNewFile();
                FileOutputStream os = new FileOutputStream(colorPath);
                if(CurrentTextViewColor!=null) {
                    os.write(color.getBytes());
                    os.close();
                }
            }catch(FileNotFoundException e){
                Log.d(TAG,"Messed up writing the file. Double check it.");
                Log.e(TAG,e.getMessage());
            }catch(IOException e){
                Log.d(TAG,"IOException can't write. Stupid android at this point.");
                Log.e(TAG,e.getMessage());
            }
    }

    public void ReadSave(){
        File colorPath;
        try{
            colorPath = Environment.getExternalStorageDirectory();
        }catch(Exception e){
            colorPath = Environment.getDataDirectory();
        }
        colorPath = new File(colorPath,getString(R.string.filedir));
        colorPath = new File(colorPath,getString(R.string.filename));
        //anti pattern, whatever
        if(colorPath.exists()){
            try {
                FileInputStream fi = new FileInputStream(colorPath);
                InputStreamReader reader = new InputStreamReader(fi);
                char[] buffer = new char[7];
                reader.read(buffer);
                StringBuilder sb = new StringBuilder();
                sb.append(buffer);
                UpdateColor(sb.toString());
            }catch(FileNotFoundException e){
                Log.e(TAG,e.getMessage());
            }catch(IOException e){
                Log.e(TAG,e.getMessage());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        stop=false;
        ColorUpdater = new Thread(){
            public void run(){
                while(!stop){
                    for(int i= 0,len=ColorPicker.getCount();i<len;i++){
                        final TextView t = (TextView) ColorPicker.getChildAt(i);
                        if(t!=null){
                            DumbDispatcher.post(new Runnable() {
                                @Override
                                public void run() {
                                    String string_color = t.getText().toString();
                                    int color = string_color == CurrentTextViewColor? white: Color.parseColor(string_color);
                                    t.setTextColor(color);
                                }
                            });
                        }
                    }
                    try {
                        Thread.sleep(1000 / 60);
                    }catch (InterruptedException e){}
                }
            }
        };
        ColorUpdater.start();

        if(CurrentTextViewColor!=null){
            UpdateColor(CurrentTextViewColor);
        }
    }

    @Override
    public void onPause(){
        stop=true;
        try {
            ColorUpdater.join();
        }catch(InterruptedException e){

        }
        super.onPause();
    }

    public void LoadColors(){
        Colors = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.colors)));
    }

    public void UnloadColors(){
        Colors=null;
    }

}
