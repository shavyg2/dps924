Part B

What i noticed from switching the SDK version is that nothing happened when i switched to a lower SDK version. 
The application still compiled and work.
Even when i was compiling with a target SDK i that didn't have installed it still worked.

I tried 15,20,21,23

I have installed 19,21,22,23

Just for kicks i tried targeting a version that was not available and the application still compiled and was lauched on the machine.
I decided to try rebuilding to see if this would change the behavior and it didn't.

Part C

init - This is to initialize your git repository. This creates the .git file and it gives a place to store all of your local git config information.

commit - This add the changes you made to your local repo. 

push - This will push the changes you have on your local account and push it to the branch. Mostlikely the master branch but this can be any branch.

add - This adds files or folders that are to be included in your git repo.

pull - This pulls the changes from the git server to your local account.

clone - This allows you to copy a repo from the server to your local machine.
